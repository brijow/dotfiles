execute pathogen#infect()
syntax on
filetype plugin indent on

"=====PUT ALL NON-PLUGIN VIM CONFIGURATIONS IN HERE

"=====CSCOPE (mappings and support from any directory)
if has('cscope')
	source $HOME/dotfiles/vim/bundle/cscope_mappings.vim
endif

"=====NERDTREE=============
map <C-n> :NERDTreeToggle<CR>

"=====remap leader \ to be ,
let mapleader = ","
"=====open vimrc in split with ',v' in normal mode
no <leader>v :vsplit $MYVIMRC<CR>

"=====automically source vimrc whenever you write
if has("autocmd")
	autocmd bufwritepost .vimrc source $MYVIMRC
endif

"=====Fill in white space with special chars
"exec "set listchars=tab:\uBB\uBB,trail:\uB7,nbsp:~"
set list
"set listchars=tab:‣\ ,trail:·
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·

"=====Source .vimrc file if it is present in working directory
"set exrc
"---sourcing vimrc from working directy is potential security hole, so set
"---secure option to restrict usage of some commands in those vimrc files
"set secure
"=====path to directories where gt should search for file under cursor
"let &path.="src/include,/usr/include/AL,"

"=====Set tabs to be 4 spaces
set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab

"=====Fix backspace problems with vim 7.4 on osx 10.10.5
set backspace=indent,eol,start

"=====Mark text beyond 81st column
"au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+',-1)

"share vim and system clipboard for easier copy pastes
set clipboard=unnamed

"search configuration
set hlsearch
hi Search ctermbg=LightGreen

"====Mouse Settings====
set mouse=a             " Enables mouse in all (a) modes
set ttymouse=xterm2     " Must be one of: xterm, xterm2, netterm, dec, jsbterm, pterm

"general configuration
"colorscheme favorites - tomorrow night, tomorrow night blue, appretice, 
" babymate256, busybee, clue, coldgreen, colorzone, 
" Chasing_Logi
colorscheme xoria256
syntax on
set laststatus=2
set number

