#!/bin/bash

# Initialize and update submodules in dotfiles
cd ~/dotfiles;
git submodule init;
echo "Submodes initialized"
git submodule update;
echo "Submodules updated";

# Create simlinks for .vim/ and .vimrc
cd ~ ;
ln -s ~/dotfiles/vim ~/.vim ;
ln -s ~/dotfiles/vimrc ~/.vimrc ;
ln -s ~/dotfiles/bash_profile ~/.bash_profile ;
echo "Created simlinks for .vim/ .vimrc and .bash_profile";
