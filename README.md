## Installation ##

### NOTE: First, rename any ~/.bash_profile, ~/.vim/ and ~/.vimrc that exist ###

### Clone to home directory and run scipt to initialize submodules and create simlinks ###

    git clone https://github.com/brijow/dotfiles.git
    cd dotfiles
	sh ./setup.sh


## Adding Plugins ##

Plugins published on github can be installed as submodules:

    cd ~/dotfiles
    git submodule add http://github.com/scrooloose/nerdtree.git vim/bundle/nerdtree
    git add .
    git commit -m "Add nerdtree plugin as submodule"

## Removing Plugins ##

	git submodule deinit vim/bundle/nerdtree
	git rm vim/bundle/nerdtree
	rm -rf .git/modules/vim/bundle/nerdtree
    git add .
    git commit -m "Remove nerdtree plugin submodule"

